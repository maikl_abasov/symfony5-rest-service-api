<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Firebase\JWT\JWT;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\UserRepository;
use App\Entity\User;

class UserController extends BaseController
{
    /**
     * @Route("/user/list/all", name="user_all")
     */
    public function index(): Response
    {
        $users = $this->findByItems(User::class);
        return $this->json(['users' => $users]);
    }

    /**
     * @Route("/user/list/clients", name="user_clients")
     */
    public function getClients(): Response
    {
        $users = $this->findByItems(User::class, ['role' => 3]);
        return $this->json(['users' => $users]);
    }

    /**
     * @Route("/user/list/managers", name="user_managers")
     */
    public function getManagers(): Response
    {
        $users = $this->findByItems(User::class, ['role' => 2]);
        return $this->json(['users' => $users]);
    }

    /**
     * @Route("/user/create", name="user_create")
     */
    public function create(Request $request): Response
    {

        $data = (array)$this->getRawData();

        $password   = password_hash(trim($data['password']), PASSWORD_DEFAULT);
        $role       = $data['role'];
        $roomId     = $data['car_showroom_id'];
        $fio        = $data['fio'];
        $email      = $data['email'];
        $phone      = $data['phone'];

        $user = new User();

        $user->setFio($fio);
        $user->setEmail($email);
        $user->setPhone($phone);
        $user->setRole($role);
        $user->setPassword($password);
        $user->setCarShowroomId($roomId);

        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();

        return $this->json([
            'user_id' => $user->getId(),
        ]);
    }

    /**
     * @Route("/user/one/{id}", name="user_by_id")
     */
    public function findUser($id) : Response
    {
        $user = $this->findItem(User::class, $id);
        return $this->json(['user' => $user]);
    }


    /**
     * @Route("/user/jwt/auth", name="user_jwt_auth")
     */
    public function auth(Request $request) : Response
    {
        $request = $this->transformJsonBody($request);
        $email    = $request->get('email');
        $password = $request->get('password');
        $user = $this->findByItems(User::class, ['email' => $email]);

        if(empty($user[0]['id']))
            return $this->json(['status' => false, 'token'  => false]);

        $user = $user[0];
        $userPwdHash = $user['password'];

        $status = $token = false;

        $verify = password_verify($password, $userPwdHash);
        if($verify) {
            $secretKey = $this->getSecretKey();
            $payload = [
                "user_id"  => $user['id'],
                "role"     => $user['role'],
                "exp"      => (new \DateTime())->modify("+5 minutes")->getTimestamp(),
            ];
            $token = JWT::encode($payload, $secretKey, 'HS256');
            $status = true;
        }

        // $this->userJwtVerify();

        return $this->json([
            'status' => $status,
            'token'  => $token,
            'user'   => $user,
        ]);
    }

}
