<?php

namespace App\Repository;

use App\Entity\Car;
use App\Entity\Order;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Order|null find($id, $lockMode = null, $lockVersion = null)
 * @method Order|null findOneBy(array $criteria, array $orderBy = null)
 * @method Order[]    findAll()
 * @method Order[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrderRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Order::class);
    }

    public function findByClientId($id) {

        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('o.id, o.create_dt, o.client_id, o.manager_id, o.car_id, o.car_showroom_id,
                          c.car_showroom_id as car_room, c.model, c.brand, c.price')
            ->from($this->getEntityName(), 'o')
            ->innerJoin(Car::class, 'c', 'WITH', 'o.car_id = c.id')
            ->where("o.client_id = $id")
            ->orderBy('o.id', 'ASC');
            // ->andWhere('o.client_id = :client_id')
            // ->setParameter('client_id', $id)
        return $qb->getQuery()->getResult();
    }

    public function formatToArray($list): array
    {
        $data = [];
        foreach ($list as $item) {
            $data[] = [
                'id'    => $item->getId(),
                'create_dt'   => $item->getCreateDt(),
                'client_id' => $item->getClientId(),
                'manager_id' => $item->getManagerId(),
                'car_id'  => $item->getCarId(),
                'car_showroom_id'  => $item->getCarShowroomId(),
            ];
        }
        return $data;
    }

}
