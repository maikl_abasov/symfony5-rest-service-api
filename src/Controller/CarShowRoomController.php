<?php

namespace App\Controller;

use App\Entity\CarShowRoom;
use App\Entity\Order;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CarShowRoomController extends BaseController
{
    /**
     * @Route("/showroom/list/all", name="showroom_all")
     */
    public function index(): Response
    {
        $repo = $this->getDoctrine()->getRepository(CarShowRoom::class);
        $list = $repo->findAll();
        $results   = $this->formatToArray($list);
        return $this->json(['list' => $results]);
    }

    /**
     * @Route("/showroom/one/{id}", name="showroom_one")
     */
    public function getCarShowRoom($id): Response
    {
        $repo = $this->getDoctrine()->getRepository(CarShowRoom::class);
        $item = $repo->find($id);
        $item = $this->formatToArray([$item]);
        return $this->json([ 'item' => $item[0] ]);
    }

    /**
     * @Route("/showroom/managers/{room_id}", name="showroom_managers")
     */
    public function getShowRoomManagers($room_id): Response
    {
        $repo = $this->getDoctrine()->getRepository(User::class);
        $list = $repo->findBy(['car_showroom_id' => $room_id]);
        $results = $repo->formatToArray($list);
        return $this->json(['users' => $results]);
    }

    /**
     * @Route("/showroom/orders/{room_id}", name="showroom_orders")
     */
    public function getShowRoomOrders($room_id): Response
    {
        $repo = $this->getDoctrine()->getRepository(Order::class);
        $list = $repo->findBy(['car_showroom_id' => $room_id]);
        $results = $repo->formatToArray($list);
        return $this->json(['orders' => $results]);
    }

    protected function formatToArray($items) {
        $data = [];
        foreach ($items as $item) {
            $data[] = [
                'id'      => $item->getId(),
                'address' => $item->getAddress(),
                'title'   => $item->getTitle(),
            ];
        }
        return $data;
    }

}
